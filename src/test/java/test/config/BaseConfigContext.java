package test.config;

import org.junit.runner.RunWith;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.util.rule.initialization.AppConfig;

/** @auth BUNCHA POOMARIN
 * @date 11/06/2558
 * @file  test.base.BaseInitials
 **/
@Lazy(true)
@Configuration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=AppConfig.class)
public abstract class BaseConfigContext {
	

}
