package test.knowledge;


import java.lang.reflect.Field;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import com.util.rule.DebugRuleAgendaEventListener;
import com.util.rule.model.RuleControlBean;


public class DroolsStatefulSession {
	final static String FILE = "D:/data/localStorage/kp.mias.pkg";
	
    public static void main(String args[]) throws Exception {
    	
        StatefulKnowledgeSession session = readKnowledgeBase().newStatefulKnowledgeSession();
     
        RuleControlBean ctb = new RuleControlBean();
        DebugRuleAgendaEventListener debugEvent = new DebugRuleAgendaEventListener( ctb );
        session.addEventListener(debugEvent);
        session.fireAllRules();
        System.out.println( "ID: "+session.getId());
      
        for(Object o : session.getObjects()) {
           createModelObject(o);
        }
        session.dispose();

    }
    private static void createModelObject(Object o) {
    	String typeName = o.getClass().getSimpleName();
    	String ocname = o.getClass().getCanonicalName();
    	String pkgName = ocname.substring(0, ocname.length() - typeName.length() - 1);
       	System.out.println( "typeName: "+typeName +", pkgName:" + pkgName );
        System.out.println("------------------");
    	for(Field str: o.getClass().getFields() ){
    		System.out.print( str.getName() );
    		System.out.println( " "+o.getClass().getTypeName() );
    	}
    }
    
    private static KnowledgeBase readKnowledgeBase() throws Exception {
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(ResourceFactory.newFileResource(FILE), ResourceType.PKG);
        KnowledgeBuilderErrors errors = kbuilder.getErrors();
        if (errors.size() > 0) {
            for (KnowledgeBuilderError error : errors) {
                System.err.println(error);
            }
            throw new IllegalArgumentException("Could not parse knowledge.");
        }

        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
        return kbase;
    }
}