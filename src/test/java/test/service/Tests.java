package test.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.util.rule.service.interfaces.IRatioFeeRuleService;

import test.config.BaseConfigContext;

public class Tests extends BaseConfigContext{

	private @Autowired IRatioFeeRuleService ratioFeeRuleService;
	@Test
	public void test() {
		System.out.println( new Date(1902, 10, 5).toString());
		
//		"EEE MMM dd HH:mm:ss Z yyyy";
		Map<String, String> input = new HashMap<String, String>();
		input.put("amount", "500000");
		input.put("requestDate", "Fri Nov 05 00:00:00 ICT 2015");
		
		Map<String, String> input2 = new HashMap<String, String>();
		input2.put("amount", "6600000");
		input2.put("requestDate", "Fri Nov 05 00:00:00 ICT 2015");
		List x = new ArrayList<>();
		x.add(input2);
		x.add(input);
		
//		List<Map<String, String>> output2 =  (List<Map<String, String>>) ratioFeeRuleService.executeRule(input);
//		List<Map<String, String>> output =  (List<Map<String, String>>) ratioFeeRuleService.executeRule(input);
		List<Map<String, String>> o =  (List<Map<String, String>>) ratioFeeRuleService.executeRule(x);
		
	
		List<Map<String, String>> listMap = new ArrayList<Map<String, String>>();
		listMap.add(new HashMap<String, String>() {
			{
				put("lgId", "1");
				put("bondsAmount", "10000");
				put("idcardNo", "124124412");
			}
		});
		listMap.add(new HashMap<String, String>() {
			{
				put("lgId", "1");
				put("bondsAmount", "10000");
				put("idcardNo", "521532534545");
			}
		});
		
		listMap.add(new HashMap<String, String>() {
			{
				put("lgId", "2");
				put("bondsAmount", "5000");
				put("idcardNo", "91716342472");
			}
		});
		
		ratioFeeRuleService.executeRule(new HashMap<String, String>() {
			{
				put("name", "test");
				put("value", "10000");
			}
		});
	}

}
