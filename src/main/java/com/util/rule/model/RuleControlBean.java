package com.util.rule.model;

import java.util.HashMap;
import java.util.Map;


public class RuleControlBean {
	private String sessionId;
	private Map<String,String> dtablePattern ;
	
	public RuleControlBean(){
		sessionId = generateId();
		this.dtablePattern = new HashMap<String, String>();
	}
	public RuleControlBean( String id ){
		this.sessionId = id;
		this.dtablePattern = new HashMap<String, String>();
	}
	
	public String getSessionId() {
		return sessionId;
	}
	public Map<String, String> getDtablePattern() {
		return dtablePattern;
	}
	public void setDtablePattern(Map<String, String> dtablePattern) {
		this.dtablePattern = dtablePattern;
	}
	
	public static String generateId(){
		return String.valueOf("sessionId: " + Math.random());
	}
}
