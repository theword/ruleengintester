package com.util.rule.model;

public class GlobalModelObject {

	private ModelObject modelObject;
	private String globalName;
	
	/**
	 * @return the globalName
	 */
	public String getGlobalName() {
		return globalName;
	}
	/**
	 * @param globalName the globalName to set
	 */
	public void setGlobalName(String globalName) {
		this.globalName = globalName;
	}
	/**
	 * @return the modelObject
	 */
	public ModelObject getModelObject() {
		return modelObject;
	}
	/**
	 * @param modelObject the modelObject to set
	 */
	public void setModelObject(ModelObject modelObject) {
		this.modelObject = modelObject;
	}
}
