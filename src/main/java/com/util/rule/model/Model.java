package com.util.rule.model;

//import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;

//import org.apache.commons.beanutils.BeanUtils;

public class Model {
	private List<ModelData> params = new ArrayList<ModelData>();

	
	public List<ModelData> getParams() {
		return params;
	}

	@XmlElement
	public void setParams(List<ModelData> params) {
		this.params = params;
	}
	
	public Map<String, String> getAsMap() {
		Map<String, String> map = new HashMap<String, String>();
		
		for(ModelData d : params) {
			map.put(d.getKey(), d.getValue());
		}
		
		return map;
	}
	
	public void convertFromMap(Map<String, String> values) {
		for(Map.Entry<String, String> e : values.entrySet())
			this.params.add(new ModelData(e.getKey(), e.getValue()));
	}
	
	
}
