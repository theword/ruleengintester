package com.util.rule.model;

import javax.xml.bind.annotation.XmlElement;

public class ModelData {
	private String key;
	private String value;
	
	public String getKey() {
		return key;
	}
	
	@XmlElement
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}
	
	@XmlElement
	public void setValue(String value) {
		this.value = value;
	}
	
	public ModelData() { }
	
	public ModelData(String k, String v) {
		this.key = k;
		this.value = v;
	}//TODO 
}
