package com.util.rule.model;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.drools.definition.type.FactField;
import org.drools.definition.type.FactType;

public class ModelObject {
    private final Logger log = Logger.getLogger(ModelObject.class);
    private final String datePattern = "EEE MMM dd HH:mm:ss Z yyyy";
    private final DateFormat format = new SimpleDateFormat( datePattern, Locale.US);
    private FactType factType = null;
    private Map<String, String> values = new HashMap<String, String>();

    public FactType getFactType() { return this.factType; }
    public void setFactType( FactType ft) {
        this.factType = ft;
    }

    public Map<String, String> getValues() { return this.values; }
    public void setValues(Map<String, String> v) {
        this.values.putAll(v);
    }

    public void setValue(String key, String val) {
        this.values.put(key, val);
    }

    public void setValuesFromObject(Object o) {
        // iterate all fields
        List<FactField> fields = this.factType.getFields();
        for(FactField f : fields) {
            String k = f.getName();
            Object ov = f.get(o);
            if(ov != null) {
                String v = ov.toString();
                this.values.put(k, v);
                log.debug("obj(" + k + ", " + v + ")");
            } else{
                log.debug("obj(" + k + ") is not found.");
            }
        }
    }

    public String getTypeName( ){
        return this.factType.getSimpleName();
    }

    public String getFactName( ){
        return this.factType.getPackageName();
    }


    public Object newFactInstance() {
        if(factType == null) {
            log.debug("Try to create a fact instance with FactType = null");
            return null;
        }

        Object o = null;
        try {
            o = factType.newInstance();
            // iterate over all fields in fv
            for(Map.Entry<String, String> e : this.values.entrySet()) {
                String field = e.getKey();
                String value = e.getValue();

                Object v = this.toFactField(factType, field, value);
                if(v != null)
                    factType.set(o, field, v);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return o;
    }

    private Object toFactField(FactType ft, String field, String value) {
        Object vo = null;
        if(value != null){
            // check and convert to proper type
            FactField ff = ft.getField(field);
            if(ff == null) {
                // Type not found
                log.debug("Filed " + field + " is not a member of " + ft.getName());
                log.debug("We will just skip.");
                return null;
            }

            try {
                log.debug("FactType: " + ft.getName() + " - Field:" + ff.getName() + " Type:" + ff.getType().getName());
                if(ff.getType().getName().compareTo("java.util.Date") == 0) {
                    // special instantiate for Date
                    vo = (Date)format.parse(value);
//					System.out.println( "Type of Date is "+vo );
                } else {
                    vo = ff.getType().getDeclaredConstructor(new Class[] {String.class}).newInstance(value);
                    log.debug("Use " + vo.getClass().getName() + " for field " + field);
                }
            } catch (InstantiationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                if(e.getCause().getClass().getSimpleName().compareTo("NumberFormatException") == 0)
                    log.debug("Seem to me that " + ff.getType().getName() + " does not like '" + value + "' -- possible incorrect format");
                else {
                    log.debug("Unrecognized Cause = " + e.getCause().getClass().getSimpleName());
                    e.printStackTrace();
                }
            } catch (NoSuchMethodException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
                log.debug("'" + value + "' is invalid date format");
            }
        }

        return vo;
    }
}
