package com.util.rule.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ModelList {
	private List<Model> models = new ArrayList<Model>();

	public List<Model> getModels() {
		return models;
	}

	@XmlElement
	public void setModels(List<Model> models) {
		this.models = models;
	}
	
	public List<Map<String, String>> getAsListMap() {
		List<Map<String, String>> lm = new ArrayList<Map<String, String>>();
		for(Model m : models) {
			lm.add(m.getAsMap());
		}
		return lm;
	}
}
