package com.util.rule.model;

import java.util.List;
import java.util.Map;

public class RuleEngineResult {

	private List<ModelObject> modelObject;
	
	private Map<String, ModelObject> globalModeMap;
	
	private List<String> ruleFiredList;

	public List<String> getRuleFiredList() {
		return ruleFiredList;
	}

	public void setRuleFiredList(List<String> ruleFiredList) {
		this.ruleFiredList = ruleFiredList;
	}

	public List<ModelObject> getModelObject() {
		return modelObject;
	}

	public void setModelObject(List<ModelObject> modelObject) {
		this.modelObject = modelObject;
	}

	public Map<String, ModelObject> getGlobalModeMap() {
		return globalModeMap;
	}

	public void setGlobalModeMap(Map<String, ModelObject> globalModeMap) {
		this.globalModeMap = globalModeMap;
	}

}
