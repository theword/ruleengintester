package com.util.rule;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.util.rule.RuleEngineCollection;
import com.util.rule.config.RulePackage;



public class RuleEngineFactory implements IRuleEngineFactory {
	
	public static final Logger log = Logger.getLogger( RuleEngineFactory.class );
	private static RuleEngineFactory instance ;
	private Map<String, RuleEngine> engines = new HashMap<String, RuleEngine>();

	@Override
	public RuleEngine getEngine( String name ) {
		if(!this.engines.containsKey(name)){
			regisNewRule(name);
		}
		return this.engines.get( name );
	}

	@Override
	public void regisNewRule(String pkname) {
		RulePackage rulePackage = RuleEngineCollection.getPackageByPkname(pkname);
		RuleEngine e = new RuleEngine(rulePackage.getChangeset());
		this.engines.put(pkname, e);
	}
	
	@Override
	public Map<String, RuleEngine> getEngines() { return this.engines; }

	@Override
	public RuleEngineFactory createInstance(){
		return  new RuleEngineFactory();
	}
	
	@Override
	public RuleEngineFactory getInstance() { 
		if( instance== null){
			instance = createInstance();
		}
		return instance;  
	}
}
