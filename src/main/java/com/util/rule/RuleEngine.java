package com.util.rule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.drools.KnowledgeBase;
import org.drools.agent.KnowledgeAgent;
import org.drools.agent.KnowledgeAgentConfiguration;
import org.drools.agent.KnowledgeAgentFactory;
import org.drools.definition.type.FactType;
import org.drools.io.ResourceFactory;
import org.drools.io.impl.UrlResource;
import org.drools.runtime.StatefulKnowledgeSession;

import com.util.rule.model.GlobalModelObject;
import com.util.rule.model.ModelObject;
import com.util.rule.model.RuleControlBean;
import com.util.rule.model.RuleEngineResult;


public class RuleEngine {

    private final Logger logger = Logger.getLogger( RuleEngine.class );
    private KnowledgeBase reBase = null;
    

    public RuleEngine(String changeset) {
       /* ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[]{"applicationContext-rule.xml"});
        ruleConfig = (RuleConfig)ctx.getBean("ruleConfig");*/

    	final String username = RuleEngineCollection.ruleConfig.getAdminUser();
    	final String password = RuleEngineCollection.ruleConfig.getAdminPassword();
        KnowledgeAgentConfiguration conf = KnowledgeAgentFactory.newKnowledgeAgentConfiguration();
        conf.setProperty ("enableBasicAuthentication", "true");
        conf.setProperty ("username", username);
        conf.setProperty ("password", password);

        KnowledgeAgent kagent = KnowledgeAgentFactory.newKnowledgeAgent("changeset", conf);
        kagent.addEventListener(new AuthKnowledgeAgentEventListener(username, password ) );
//        logger.info( changeset );
        UrlResource url = (UrlResource) ResourceFactory.newUrlResource(changeset);
        url.setBasicAuthentication("enabled");
        url.setUsername( username );
        url.setPassword( password );

        kagent.applyChangeSet( url);

        this.reBase = kagent.getKnowledgeBase();
    }

    public void insertModelObjectAsFact( StatefulKnowledgeSession session, ModelObject mo ) {
        Object o = mo.newFactInstance();
        if( o != null ) {
        	logger.info("Create Fact Instance from ModelObject: ");
            session.insert(o);
        } else {
            logger.info("cannot create Fact Instance from ModelObject: " + mo);
        }
    }

    public RuleEngineResult execute(List<ModelObject> models, List<GlobalModelObject> globalModels, RuleControlBean rcb) {
        // create session
        StatefulKnowledgeSession session = this.reBase.newStatefulKnowledgeSession();

        /* Debug drools while execute */
		DebugRuleAgendaEventListener debugEvent = new DebugRuleAgendaEventListener(rcb);
		session.addEventListener(debugEvent);
		
        // insert global model parameter
        for(GlobalModelObject glo : globalModels) {
            session.setGlobal(glo.getGlobalName(), glo.getModelObject().newFactInstance());
        }

        logger.info("Size of model fact object: " + models!=null?models.size():"Null" );
        // insert all facts
        for(ModelObject mo : models) {
            this.insertModelObjectAsFact(session, mo);
        }
        // execute rules
        session.fireAllRules();
        RuleEngineResult result = new RuleEngineResult();

        List< ModelObject > obj = new ArrayList< ModelObject >();
        for(Object o : session.getObjects()) {
            ModelObject mo1 =  this.createModelObject(o);
            obj.add(mo1);
        }
        result.setModelObject(obj);

        Map<String, ModelObject > globals = new HashMap< String, ModelObject >();
        for(GlobalModelObject glo : globalModels) {
            Object o = session.getGlobal(glo.getGlobalName());
            ModelObject mo1 =  this.createModelObject(o);
            globals.put(glo.getGlobalName(), mo1);
        }
        result.setGlobalModeMap(globals);
//		result.setRuleFiredList(debugEvent.getRuleFiredList());

//		this.reBase = null;
        session.dispose();
        return result;
    }

    public FactType getFactType(String pkg, String type) {
        return this.reBase.getFactType(pkg, type);
    }

    public ModelObject createModelObject(Object o) {
        String typeName = o.getClass().getSimpleName();
        String ocname = o.getClass().getCanonicalName();
        String pkgName = ocname.substring(0, ocname.length() - typeName.length() - 1);
        ModelObject mo = new ModelObject();
        mo.setFactType( this.getFactType(pkgName, typeName));
        mo.setValuesFromObject(o);
        return mo;
    }
}