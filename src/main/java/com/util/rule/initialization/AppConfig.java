package com.util.rule.initialization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.util.rule.config.RuleConfig;

@Lazy(true)
@Configuration
@ComponentScan({"com.util.rule"})
@PropertySource("classpath:ApplicationResource.properties")
public abstract  class AppConfig {
	
	public @Autowired Environment environment;
	
	@Bean
	public RuleConfig ruleConfig(){
		RuleConfig ruleConfig = new RuleConfig();
		ruleConfig.setAdminPassword( environment.getProperty("adminPassword") );
		ruleConfig.setAdminUser( environment.getProperty("adminUser") );
		ruleConfig.setRefreshRule( environment.getProperty("refreshRule") );
		ruleConfig.setServerUrl( environment.getProperty("serverUrl") );
		ruleConfig.setStateRule( environment.getProperty("stateRule") );
		return ruleConfig;
	}
	
	
}
