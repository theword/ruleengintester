package com.util.rule.initialization;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.util.rule.RuleEngineCollection;
import com.util.rule.config.RulePackage;

public class AppRun {
	

	public static void main( String[] args) throws IOException{
		AnnotationConfigApplicationContext  ctx = new AnnotationConfigApplicationContext(AppConfig.class);
		final Date start = new Date();
		List<RulePackage> pkgs =  RuleEngineCollection.ruleCollection.getRulePackage();
		ctx.close();
		System.out.println( String.format("Used time to initial: %f s.", Double.valueOf((new Date().getTime() - start.getTime()))/1000) );
		for( RulePackage pkg : pkgs ) {
			System.out.println( pkg.getTitle() + "\t\t\t\t\t" + pkg.getRuleMetadata().getState()  );
			getPackage( pkg.getTitle() );
		}
	}
	
	static void getPackage( String pkgName) throws IOException{
		String urlString = "http://localhost:8181/guvnor/rest/packages/"+pkgName+"/binary";
		String username = "admin";
		String password = "admin";
		Authenticator.setDefault(new MyAuthenticator(username, password));
		URL url = new URL(urlString);
	
		InputStream content = (InputStream) url.getContent();
		byte[] buffer = new byte[content.available()];
		content.read(buffer);
	 
	    File targetFile = new File("D:/data/localStorage/"
	    		+pkgName+ ".pkg");
	    OutputStream outStream = new FileOutputStream(targetFile);
	    outStream.write(buffer);
	    
	    content.close();
	    outStream.close();
	    
		System.out.println("Done.");
	}


 static class MyAuthenticator extends Authenticator {
   private String username, password;

   public MyAuthenticator(String user, String pass) {
     username = user;
     password = pass;
   }

   protected PasswordAuthentication getPasswordAuthentication() {
//     System.out.println("Requesting Host  : " + getRequestingHost());
//     System.out.println("Requesting Port  : " + getRequestingPort());
//     System.out.println("Requesting Prompt : " + getRequestingPrompt());
//     System.out.println("Requesting Protocol: "
//         + getRequestingProtocol());
//     System.out.println("Requesting Scheme : " + getRequestingScheme());
//     System.out.println("Requesting Site  : " + getRequestingSite());
     return new PasswordAuthentication(username, password.toCharArray());
   }
 }
}
