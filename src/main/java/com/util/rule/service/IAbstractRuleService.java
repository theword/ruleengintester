package com.util.rule.service;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import com.util.rule.IRuleEngineFactory;
import com.util.rule.RuleEngine;

public interface IAbstractRuleService extends IRuleEngineFactory {
		/**
		 * 
		 */
	    public void initRuleService( String pkgName);
	    /**
	     * 
	     * @param sessionId
	     * @return
	     */
	    //public RuleEngineResult execute(String sessionId);
	    /**
	     * 
	     * @param o
	     * @return
	     */
//	    public ModelObject createModelObject(Object o);
	    /**
	     * 
	     * @return
	     */
//	    public String generateRuleId();
	    /**
	     * 
	     * @return
	     */
	  /*  public Map<String, String> getDtablePattern();
	    *//**
	     * 
	     * @param dtablePattern
	     *//*
	    public void setDtablePattern(Map<String, String> dtablePattern);*/
	    /**
	     * 
	     * @param map
	     * @param o
	     * @return
	     * @throws IllegalAccessException
	     * @throws InvocationTargetException
	     */
	    public Object mapToBean( Map<String, String> map, Object o ) throws IllegalAccessException, InvocationTargetException;
	    /**
	     * 
	     * @param someBean
	     * @return
	     * @throws IllegalAccessException
	     * @throws InvocationTargetException
	     * @throws NoSuchMethodException
	     */
	    public Map<String, String> beanToMap( Object someBean ) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException;
	    /**
	     * 
	     * @return
	     */
	    public String getPkgName();
	    /**
	     * 
	     * @param pkgName
	     */
	    public void setPkgName(String pkgName);
	    /**
	     * 
	     * @return
	     */
	    public RuleEngine getRuleEngine();
	    /**
	     * 
	     * @param ruleEngine
	     */
	    public void setRuleEngine(RuleEngine ruleEngine);
	    
	    /**
	     * 
	     * @param dataModel
	     * @param modelType
	     * @return
	     */
//	    public Boolean addObjectModel(Map<String, String> dataModel, String modelType );
	    
	    /**
	     * 
	     * @param dataModel
	     * @param modelType
	     * @return
	     * @throws IllegalAccessException
	     * @throws InvocationTargetException
	     * @throws NoSuchMethodException
	     */
//	    @SuppressWarnings("rawtypes")
//		public Boolean addListObjectModel( List dataModel, String modelType );
	    
	    /**
	     * 
	     * @param dataModel
	     * @param modelType
	     * @param globalName
	     * @return
	     */
//	    public Boolean addGlobalObjectModel(Map<String, String> dataModel, String modelType , String globalName);
		

}
