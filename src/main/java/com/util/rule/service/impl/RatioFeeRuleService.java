package com.util.rule.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.util.rule.constants.RuleConstants;
import com.util.rule.model.ModelObject;
import com.util.rule.model.RuleEngineResult;
import com.util.rule.service.AbstractRuleService;
import com.util.rule.service.interfaces.IRatioFeeRuleService;

@Service("ratioFeeRuleService")
public class RatioFeeRuleService extends AbstractRuleService implements IRatioFeeRuleService{

	private final Logger log = Logger.getLogger(RatioFeeRuleService.class);
	private final String DEFAULT_RULE = "Test";
	
	@Override
	public List<?> executeRule(Map<String, String> input) {
		List<Map<String, String>> feeOutputList = new ArrayList<Map<String, String>>();
		final String sessionId = "sessionId:"+String.valueOf(Math.random());
		try {
			super.initRuleService(DEFAULT_RULE);
			super.addObjectModel(input, RuleConstants.ModelType.VARIABLE);
			RuleEngineResult result = super.execute(sessionId);
			List<ModelObject> modelOutput = result.getModelObject();
			for (ModelObject o : modelOutput) {
				log.info(" Output Factype: " +  o.getTypeName());
				log.info(" ,Data: " +  o.getValues());
				
				if (RuleConstants.ModelType.OUTPUT.equals(o.getTypeName())) {
					feeOutputList.add(o.getValues());
				} 
			}
		}catch( Exception e ){
			log.error(e.getMessage());
			
		}
		
		return feeOutputList;
	}
	
	public void groupping ( List<Map<String, String>> input ){
		final String pk = "groupping";
		
		final String sessionId = "sessionId:"+String.valueOf(Math.random());
		Map<String, String> variable = new HashMap<>();
		variable.put("name", "result");
		variable.put("id", "START");
		try {
			super.initRuleService(pk);
			super.addObjectModel(variable, RuleConstants.ModelType.VARIABLE);
			super.addListObjectModel(input, RuleConstants.ModelType.INPUT);
			RuleEngineResult result = super.execute(sessionId);
			List<ModelObject> modelOutput = result.getModelObject();
			for (ModelObject o : modelOutput) {
				System.out.print(" Output Factype: " +  o.getTypeName());
				System.out.println(" ,Data: " +  o.getValues());
			}
		}catch( Exception e ){
			log.error(e.getMessage());
			
		}
	}
	
	
	public List<?> executeRule( List<Map<String, String>> input){
		List<Map<String, String>> feeOutputList = new ArrayList<Map<String, String>>();
		try {
			super.initRuleService(DEFAULT_RULE);
			super.addListObjectModel(input, RuleConstants.ModelType.INPUT);
			RuleEngineResult result = super.execute(generateRuleId());
			List<ModelObject> modelOutput = result.getModelObject();
			for (ModelObject o : modelOutput) {
				System.out.print("Factype: " +  o.getTypeName());
				System.out.println(" ,Data: " +  o.getValues());
				
				if (RuleConstants.ModelType.OUTPUT.equals(o.getTypeName())) {
					feeOutputList.add(o.getValues());
				} 
			}
			
			
		}catch( Exception e ){
			log.error(e.getMessage());
			
		}
		
		return feeOutputList;
	}
}
