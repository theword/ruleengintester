package com.util.rule.service;


import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.beanutils.BeanUtils;

import com.util.rule.RuleEngine;
import com.util.rule.RuleEngineFactory;
import com.util.rule.model.GlobalModelObject;
import com.util.rule.model.ModelObject;
import com.util.rule.model.RuleControlBean;
import com.util.rule.model.RuleEngineResult;



public abstract class AbstractRuleService extends RuleEngineFactory implements IAbstractRuleService{
	
    private String pkgName;
    private RuleEngine ruleEngine;
    private List<ModelObject> modelList;
    private List<GlobalModelObject> globalModelList;
//    private Map<String,String> dtablePattern;

      
    public AbstractRuleService(){
    
    }
    
    private void createModel(){
    	 this.modelList = new ArrayList<ModelObject>();
         this.globalModelList = new ArrayList<GlobalModelObject>();
//         this.dtablePattern = new HashMap<String, String>();
    }
    
    @Override
    public void initRuleService( String pkgName ){
    	log.info("Initial rule service");
    	this.createModel();
    	this.setPkgName(pkgName);
        log.info("Set rule package name'" +getPkgName()+"'");
        this.ruleEngine = super.getInstance().getEngine(getPkgName());
        if ( this.ruleEngine == null){
            log.debug( "NullPointerException Rule service not found.");
            System.exit(0);
        }
        log.info("Initialization rule service complete.");
    }
    
//    @Override
    protected Boolean addObjectModel(Map<String, String> dataModel, String modelType ) {
        try{
            // convert params to ModelObjects
            ModelObject mo = new ModelObject();
            log.debug("Add object model. type '"+modelType+"'");
            mo.setFactType( this.ruleEngine.getFactType(pkgName, modelType) );
            if(dataModel!=null){
                mo.setValues( dataModel );
                log.debug("verify parameter of model type: "+ dataModel.entrySet());
            }else{
                log.debug("Data model is null.");
            }

            this.modelList.add(mo);
        }catch(Exception ex){
            log.error(ex.getMessage());
            return false;
        }
        return true;
    }
    
//    @Override
    protected Boolean addGlobalObjectModel(Map<String, String> dataModel, String modelType , String globalName) {
        try{
            // convert params to ModelObjects
            ModelObject mo = new ModelObject();
            mo.setFactType( this.ruleEngine.getFactType( pkgName, modelType ) );
            if(dataModel!=null)	mo.setValues( dataModel );


            GlobalModelObject glo = new GlobalModelObject();
            glo.setModelObject(mo);
            glo.setGlobalName(globalName);
            this.globalModelList.add(glo);

            return true;

        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
    
    protected RuleEngineResult execute(String sessionId) {
        try{
            // now we execute the engine
            log.info( "sessionId : "+ sessionId );
       
            RuleEngineResult results = this.ruleEngine.execute(this.modelList , this.globalModelList,  new RuleControlBean(sessionId) );
            return results;
        }catch(Exception ex){
            System.out.println( "error law la "+ex.getMessage());
            ex.printStackTrace();
            return null;
        }
    }
    
//    @Override
    protected ModelObject createModelObject(Object o) {
        String typeName = o.getClass().getSimpleName();
        String ocname = o.getClass().getCanonicalName();
        String pkgName = ocname.substring(0, ocname.length() - typeName.length() - 1);
        ModelObject mo = new ModelObject();
        mo.setFactType( this.ruleEngine.getFactType(pkgName, typeName));
        mo.setValuesFromObject(o);
        return mo;
    }
    
//    @Override
    protected String generateRuleId(){
        return "sessionId:"+String.valueOf(Math.random());
    }
    
//    @Override
//    public Map<String, String> getDtablePattern() {
//        return dtablePattern;
//    }
//    
//    @Override
//    public void setDtablePattern(Map<String, String> dtablePattern) {
//        this.dtablePattern = dtablePattern;
//    }
    
    @Override
    public Object mapToBean( Map<String, String> map, Object o ) throws IllegalAccessException, InvocationTargetException {
        BeanUtils.populate( o , map  );
        return o;
    }
    
    @Override
    public Map<String, String> beanToMap( Object someBean ) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{
    	Map<String, String> properties;
    	if( someBean == null ){
    		return null;
    	}else if ( someBean instanceof Set<?> || 
    			someBean instanceof Collection<?> ||
    			someBean instanceof Map<?,?>  || 
    			someBean instanceof HashMap<?,?> || 
    			someBean instanceof TreeMap<?,?> ||
    			someBean instanceof Hashtable<?,?> ||
    			someBean instanceof SortedMap<?,?> ||
    			someBean instanceof HashMap<?,?>  ){
    		
    		 return (Map<String, String>) someBean;
    	}else{
    		 return BeanUtils.describe(someBean); // access properties as Map
    	}
    	
    }

    /**
     *
     * @param packageName
     * @return dropDownList
     */
  /*  public static List<DropdownList> getPackageByPackageName( String packageName ) {
//        List<RulePackage> rulePackages = ruleEngineCollection.getListRulePkByParent( packageName );
        List<RulePackage> rulePackages = RuleEngineCollection.getListRulePkByParent( packageName );
        List<DropdownList> list = new ArrayList<DropdownList>();
        DropdownList dropdown = null;
        if( rulePackages!=null ){
            for(RulePackage rulePackage : rulePackages){
                dropdown = new DropdownList();
                dropdown.setId(rulePackage.getTitle());
                dropdown.setDesc(rulePackage.getDescription());
                ERROR IF RULE PACKAGE IS RETURN NULLITY LINK BY BUNCHA POOMARIN 
                try{
                    if( !Utility.isNull( rulePackage.getMainLink() ))
                        dropdown.setDesc2(rulePackage.getMainLink());
                }catch( NullPointerException er ){
//                	er.getStackTrace();
                    log.error("Nullpointer Exception between get child package from: " + packageName);
                }
                list.add(dropdown);
            }
        }
        return list;
    }*/
    
    

    /**
     *
     * @param packageName
     * @return List<DropdownList>
     */
    /*public List<DropdownList> getDropDownListFromRule( String packageName ) {
//        List<RulePackage> rulePackages = ruleEngineCollection.getListRulePkByParent( packageName );
        List<RulePackage> rulePackages = RuleEngineCollection.getListRulePkByParent( packageName );
        List<DropdownList> list = new ArrayList<DropdownList>();
       System.out.println( packageName );
        if( rulePackages!=null ){
            for(RulePackage rulePackage : rulePackages){
            	DropdownList dropdown = new DropdownList();
                dropdown.setId(rulePackage.getTitle());
                dropdown.setDesc(rulePackage.getDescription());
                ERROR IF RULE PACKAGE IS RETURN NULLITY LINK BY BUNCHA POOMARIN 
                try{
                    if( !Utility.isNull( rulePackage.getMainLink() ))
                        dropdown.setDesc2(rulePackage.getMainLink());
                }catch( NullPointerException er ){
                	log.error("Nullpointer Exception between get child package from: " + packageName);
                }

                list.add(dropdown);
            }
        }
        return list;
    }*/
    
//	@Override
	@SuppressWarnings("rawtypes")
	protected Boolean addListObjectModel( List dataModel, String modelType ) {
		try {
			for( Object o: dataModel){
				Map<String, String> m;
				m = beanToMap(o);
				addObjectModel(m, modelType);
			}
		} catch ( IllegalAccessException e ) {
			e.printStackTrace();
			log.error(e.getMessage());
            return false;
		} catch ( InvocationTargetException e ) {
			e.printStackTrace();
			log.error(e.getMessage());
            return false;
		} catch ( NoSuchMethodException e ) {
			e.printStackTrace();
			log.error(e.getMessage());
            return false;
		}
		return true;
	}
	
	@Override
    public String getPkgName() {
        return pkgName;
    }
    @Override
    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }
    @Override
    public RuleEngine getRuleEngine() {
        return ruleEngine;
    }
    @Override
    public void setRuleEngine(RuleEngine ruleEngine) {
        this.ruleEngine = ruleEngine;
    }

}
