package com.util.rule.service.interfaces;

import java.util.List;
import java.util.Map;

import com.util.rule.service.IAbstractRuleService;

public interface IRatioFeeRuleService extends IAbstractRuleService{

	public List<?> executeRule(Map<String, String> input);
	public List<?> executeRule( List<Map<String, String>> input);
	
	public void groupping( List<Map<String, String>> input );

}
