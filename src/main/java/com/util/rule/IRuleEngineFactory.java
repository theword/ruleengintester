package com.util.rule;

import java.util.Map;


public interface IRuleEngineFactory {

	void regisNewRule(String pkname);
	RuleEngineFactory createInstance();
	RuleEngineFactory getInstance();
	Map<String, RuleEngine> getEngines();
	RuleEngine getEngine(String name);
}
