package com.util.rule.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@XmlRootElement(name="config")
public class Config {
	private static final Logger logger = LoggerFactory.getLogger(Config.class);
	private List<RuleConfig> rules;
	
	public List<RuleConfig> getRules() { return this.rules; }
	
	@XmlElement(name="rule")
	public void setRules(List<RuleConfig> rules) { this.rules = rules; }
	
	public static Config newInstance(InputStream is) {
		Config config = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Config.class);
			 
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			config = (Config) jaxbUnmarshaller.unmarshal(is);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return config;
	}

	public static Config newInstance(String filename) {
		Config config = null;
		try {
			String workingDir = System.getProperty("user.dir");
			logger.debug("Current working directory : " + workingDir);
			InputStream is = new FileInputStream(filename);
			config = newInstance(is);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return config;
	}
}
