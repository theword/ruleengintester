package com.util.rule.config;

public class RuleConfig {

    private String serverUrl;
    private String adminUser;
    private String adminPassword;
    private String refreshRule;
    private String stateRule;

    /**
     * @return the serverUrl
     */
    public String getServerUrl() {
        return serverUrl;
    }
    /**
     * @param serverUrl the serverUrl to set
     */
    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }
    /**
     * @return the adminUser
     */
    public String getAdminUser() {
        return adminUser;
    }
    /**
     * @param adminUser the adminUser to set
     */
    public void setAdminUser(String adminUser) {
        this.adminUser = adminUser;
    }
    /**
     * @return the adminPassword
     */
    public String getAdminPassword() {
        return adminPassword;
    }
    /**
     * @param adminPassword the adminPassword to set
     */
    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }
    /**
     * @return the refreshRule
     */
    public String getRefreshRule() {
        return refreshRule;
    }
    /**
     * @param refreshRule the refreshRule to set
     */
    public void setRefreshRule(String refreshRule) {
        this.refreshRule = refreshRule;
    }
    /**
     * @return the stateRule
     */
    public String getStateRule() {
        return stateRule;
    }
    /**
     * @param stateRule the stateRule to set
     */
    public void setStateRule(String stateRule) {
        this.stateRule = stateRule;
    }

    public String  toString() {
		return  "Server url: " + serverUrl + "\n"
				+ "Admin user: " + adminUser + "\n"
				+ "Password: " + adminPassword + "\n"
				+ "Refresh Rule active: " + refreshRule + "\n"
				+ "State Rule : " + stateRule;
	}

}
