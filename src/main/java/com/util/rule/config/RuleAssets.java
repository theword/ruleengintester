package com.util.rule.config;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "asset")
public class RuleAssets {

	private String description;
	private RuleMetadata ruleMetadata;
	private String published;
	private String title;

	public String getDescription() {
		return description;
	}
	
	@XmlElement(name="description")
	public void setDescription(String description) {
		this.description = description;
	}

	public RuleMetadata getRuleMetadata() {
		return ruleMetadata;
	}
	
	@XmlElement(name="metadata")
	public void setRuleMetadata(RuleMetadata ruleMetadata) {
		this.ruleMetadata = ruleMetadata;
	}

	public String getPublished() {
		return published;
	}

	@XmlElement(name="published")
	public void setPublished(String published) {
		this.published = published;
	}

	public String getTitle() {
		return title;
	}

	@XmlElement(name="title")
	public void setTitle(String title) {
		this.title = title;
	}

}
