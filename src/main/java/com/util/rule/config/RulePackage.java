package com.util.rule.config;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import org.apache.log4j.Logger;

import com.util.rule.RuleEngineCollection;
import com.util.rule.constants.RuleConstants;

public class RulePackage  implements IRulePackage{

	protected final Logger log = Logger.getLogger( RulePackage.class );
	private String author;
	private List<String> ruleAssetsString;
	private List<RuleAssets> ruleAssets;
	private String binaryLink;
	private String description;
	private RuleMetadata ruleMetadata;
	private String published;
	private String title;
	
	private RuleAssets mainAsset;
	private String mainLink;
	

	public String getChangeset() {
		String changeset = new String(binaryLink);
		changeset = changeset.replace("/rest/packages/", "/org.drools.guvnor.Guvnor/package/");
		changeset = changeset.replace("/binary", "/LATEST/ChangeSet.xml");
		return changeset;
	}

	public String getAuthor() {
		return author;
	}

	@XmlElement(name = "author")
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getBinaryLink() {
		return binaryLink;
	}

	@XmlElement(name = "binaryLink")
	public void setBinaryLink(String binaryLink) {
		this.binaryLink = binaryLink;
	}

	public String getDescription() {
		if(description == null){
			description = title;
		}
		return description;
	}

	@XmlElement(name = "description")
	public void setDescription(String description) {
		this.description = description;
	}

	public RuleMetadata getRuleMetadata() {
		return ruleMetadata;
	}

	@XmlElement(name = "metadata")
	public void setRuleMetadata(RuleMetadata ruleMetadata) {
		this.ruleMetadata = ruleMetadata;
	}

	public String getPublished() {
		return published;
	}

	@XmlElement(name = "published")
	public void setPublished(String published) {
		this.published = published;
	}

	public String getTitle() {
		return title;
	}

	@XmlElement(name = "title")
	public void setTitle(String title) {
		this.title = title;
	}
	
	public List<String> getRuleAssetsString() {
		return ruleAssetsString;
	}

	@XmlElement(name = "assets")
	public void setRuleAssetsString(List<String> ruleAssetsString) {
		this.ruleAssetsString = ruleAssetsString;
	}

	@Override
 	public List<RuleAssets> getRuleAssets() {
		if(ruleAssets==null){
			ruleAssets = RuleEngineCollection.getRuleAssesDetail(ruleAssetsString);
		}
		return ruleAssets;
	}

	@Override
	public RuleAssets getMainAsset() {
		if(mainAsset==null){
			mainAsset = RuleEngineCollection.getMainAsses(this.getRuleAssets(), title);
		}
		return mainAsset;
	}

	@Override
	public String getMainLink() {
		if(mainLink==null){
			final String serverUrl = RuleEngineCollection.ruleConfig.getServerUrl();
			mainLink = 	String.format( serverUrl +RuleConstants.URL.LINK_RULE, this.getMainAsset().getRuleMetadata().getUuid());	
		}
		return mainLink;
	}
}
