package com.util.rule.config;

import java.util.List;


public interface IRulePackage {

	List<RuleAssets> getRuleAssets();

	RuleAssets getMainAsset();

	String getMainLink();

}
