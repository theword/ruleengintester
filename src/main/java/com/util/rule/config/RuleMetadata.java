package com.util.rule.config;

import javax.xml.bind.annotation.XmlElement;

public class RuleMetadata {

	private String archived;
	private String checkinComment;
	private String created;
	private String state;
	private String uuid;
	private String versionNumber;
	private String format;
	
	
	public String getArchived() {
		return archived;
	}

	@XmlElement(name = "archived")
	public void setArchived(String archived) {
		this.archived = archived;
	}

	public String getCheckinComment() {
		return checkinComment;
	}

	@XmlElement(name = "checkinComment")
	public void setCheckinComment(String checkinComment) {
		this.checkinComment = checkinComment;
	}

	public String getCreated() {
		return created;
	}

	@XmlElement(name = "created")
	public void setCreated(String created) {
		this.created = created;
	}

	public String getState() {
		return state;
	}

	@XmlElement(name = "state")
	public void setState(String state) {
		this.state = state;
	}

	public String getUuid() {
		return uuid;
	}

	@XmlElement(name = "uuid")
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	@XmlElement(name = "versionNumber")
	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getFormat() {
		return format;
	}

	@XmlElement(name = "format")
	public void setFormat(String format) {
		this.format = format;
	}

}
