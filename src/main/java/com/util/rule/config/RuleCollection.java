package com.util.rule.config;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "collection")
public class RuleCollection {

	private List<RulePackage> rulePackage;

	public List<RulePackage> getRulePackage() {
		return rulePackage;
	}

	@XmlElement(name = "package")
	public void setRulePackage(List<RulePackage> rulePackage) {
		this.rulePackage = rulePackage;
	}
	
}
