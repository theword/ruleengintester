package com.util.rule;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.client.apache.ApacheHttpClient;
import com.util.rule.config.RuleAssets;
import com.util.rule.config.RuleCollection;
import com.util.rule.config.RuleConfig;
import com.util.rule.config.RulePackage;
import com.util.rule.constants.RuleConstants;



@Component("ruleEngineCollection")
public class RuleEngineCollection{
	
	private static final Logger log = Logger.getLogger( RuleEngineCollection.class );
	private static final int CONNECT_TIME_OUT = 3000;
	private static final Boolean FOLLOW_REDIRECTS = false;
	private static final int THREADPOOL_SIZE = 10; 
	
	private static int length = 0;
	public static RuleCollection ruleCollection;
	public static RuleConfig ruleConfig;

	@Autowired
	public void initialization(  RuleConfig ruleConfig  ){

		RuleEngineCollection.ruleConfig = ruleConfig;
		initialization();
	}

	private static ClientConfig getClientConfig(){
		ClientConfig config = new DefaultClientConfig();
		config.getProperties().put(ClientConfig.PROPERTY_THREADPOOL_SIZE,THREADPOOL_SIZE);
		config.getProperties().put(ClientConfig.PROPERTY_CONNECT_TIMEOUT,CONNECT_TIME_OUT);
		config.getProperties().put(ClientConfig.PROPERTY_FOLLOW_REDIRECTS,FOLLOW_REDIRECTS);
		return config;
	}
	
	private static HTTPBasicAuthFilter basicAuthFilter( final String username, final String password){
		return new HTTPBasicAuthFilter(username, password);
	}
	private static ApacheHttpClient getClientResource(){
		ApacheHttpClient client = ApacheHttpClient.create(getClientConfig());
		client.addFilter( basicAuthFilter(RuleEngineCollection.ruleConfig.getAdminUser(),
												RuleEngineCollection.ruleConfig.getAdminPassword()));
		client.addFilter(new LoggingFilter());
		return client;
	}
	private static WebResource buildWebResource(){
		return getClientResource().resource(UriBuilder.fromUri(RuleEngineCollection.ruleConfig.getServerUrl()+RuleConstants.URL.LIST_PACKAGE).build());
	}
	
	private static void initialization(){
		try{
			WebResource.Builder builder = buildWebResource().getRequestBuilder();
			ClientResponse serviceResponse = builder.accept("application/xml").get(ClientResponse.class);
			if( RuleEngineCollection.ruleCollection == null || (serviceResponse.getLength() != -1 && length != serviceResponse.getLength())){
				String responseString = serviceResponse.getEntity(String.class);
				length = serviceResponse.getLength();

				JAXBContext jaxbContext = JAXBContext.newInstance(RuleCollection.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

				StringReader reader = new StringReader(responseString);
				RuleEngineCollection.ruleCollection = (RuleCollection) jaxbUnmarshaller.unmarshal(reader);
			}
		}catch(ClientHandlerException e){
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public static void refreshInstance() {
		initialization();
	}

	public static RuleCollection getCollection() {
		if( RuleEngineCollection.ruleCollection == null || RuleEngineCollection.ruleConfig.getRefreshRule().equals("true")){
			log.info("RuleCollection is undefined or Refresh rule is activated");
			initialization();
		}
		return RuleEngineCollection.ruleCollection;
	}

	public static RulePackage getPackageByPkname(String pkname) {
		RulePackage rulePackage = null;
		List<RulePackage> rulePackages = getCollection().getRulePackage();
		for(RulePackage obj : rulePackages)	{
			String ruleState = obj.getRuleMetadata().getState();
			if(obj.getTitle().equalsIgnoreCase(pkname)
					&& RuleEngineCollection.ruleConfig.getStateRule().equalsIgnoreCase(ruleState)){
				rulePackage = obj;
				break;
			}
		}
		if( rulePackage == null ){
			log.warn("This package '"+pkname+"' is no valid or inactive. Please verify.");
		}
		return rulePackage;
	}

	public RulePackage getPackageByUUID(String uuid) {
		RulePackage rulePackage = null;
		List<RulePackage> rulePackages = getCollection().getRulePackage();
		for(RulePackage obj : rulePackages){
			String ruleState = obj.getRuleMetadata().getState();
			if(obj.getRuleMetadata().getUuid().equalsIgnoreCase(uuid) && RuleEngineCollection.ruleConfig.getStateRule().equalsIgnoreCase(ruleState)){
				rulePackage = obj;
			}else{
				log.warn("This uuid '"+uuid+"' is no valid or inactive. Please verify.");
			}
		}
		return rulePackage;
	}

	public static List<RulePackage> getListRulePkByParent( String pkparent ){
		List<RulePackage> result = new ArrayList<RulePackage>();
		List<RulePackage> rulePackages = getCollection().getRulePackage();
		for(RulePackage obj : rulePackages)		{
			String ruleState = obj.getRuleMetadata().getState();
			if(obj.getTitle().startsWith(pkparent+".")  && RuleEngineCollection.ruleConfig.getStateRule().equalsIgnoreCase(ruleState)){
				result.add(obj);
			}
		}
		return result;
	}


	public static List<RuleAssets> getRuleAssesDetail(List<String> ruleAssets){
		List<RuleAssets> result = new ArrayList<RuleAssets>();
		try{
			ClientConfig config = new DefaultClientConfig();
			ApacheHttpClient client = ApacheHttpClient.create(config);

			String username = RuleEngineCollection.ruleConfig.getAdminUser();
			String password = RuleEngineCollection.ruleConfig.getAdminPassword();
			final HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(username, password);
			client.addFilter(authFilter);
			client.addFilter(new LoggingFilter());

			WebResource webResource = null;
			WebResource.Builder builder = null;
			ClientResponse serviceResponse = null;
			String responseString = null;
			StringReader reader  = null;
			RuleAssets asset = null;
			JAXBContext jaxbContext = JAXBContext.newInstance(RuleAssets.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			if(ruleAssets != null){
				for(String ruleAsset : ruleAssets){
					try{
						webResource = client.resource(UriBuilder.fromUri(ruleAsset).build());
						builder = webResource.getRequestBuilder();
						serviceResponse = builder.accept("application/xml").get(ClientResponse.class);
						responseString = serviceResponse.getEntity(String.class);
						reader = new StringReader(responseString);
						asset = (RuleAssets) jaxbUnmarshaller.unmarshal(reader);
						if(asset.getRuleMetadata().getFormat().equals(RuleConstants.AssesFormat.GDST) || asset.getRuleMetadata().getFormat().equals(RuleConstants.AssesFormat.BRL)){
							result.add(asset);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return result;
	}

	public static RuleAssets getMainAsses(List<RuleAssets> ruleAssets, String pkName){
		RuleAssets asset = null;
		boolean isFound = false;
		if(ruleAssets != null){
			for(RuleAssets obj : ruleAssets){ // find by package name
				if(pkName.contains(obj.getTitle())){
					isFound = true;
					asset = obj;
					break;
				}
			}
			if(!isFound){
				for(RuleAssets obj : ruleAssets){ // find by rule type Decistion table
					if(obj.getRuleMetadata().getFormat().equals(RuleConstants.AssesFormat.GDST)){
						isFound = true;
						asset = obj;
						break;
					}
				}
				if(!isFound){
					for(RuleAssets obj : ruleAssets){ // find by rule type business rule
						if(obj.getRuleMetadata().getFormat().equals(RuleConstants.AssesFormat.BRL)){
							isFound = true;
							asset = obj;
							break;
						}
					}
				}
			}
		}
		return asset;
	}

	public final void setRuleConfig( RuleConfig ruleConfig ){
		RuleEngineCollection.ruleConfig = ruleConfig;
	}

	public final RuleConfig getRuleConfig( ){
		return RuleEngineCollection.ruleConfig ;
	}
}
