package com.util.rule.constants;

public class RuleConstants {
	public static final Integer TRUE = 1;
	public static final Integer FALSE = 0;
	public final static String FILE_SEPARATOR = System.getProperty("file.separator");
	public final static String LINE_SEPARATOR = System.getProperty("line.separator");

	public class URL {
		public static final String LIST_PACKAGE = "/rest/packages/";
		public static final String LINK_RULE = "/org.drools.guvnor.Guvnor/standaloneEditorServlet?assetsUUIDs=%s&client=oryx";
	}

	public class AssesFormat {
		public static final String GDST = "gdst";
		public static final String MODEL = "model.drl";
		public static final String FUNCTION = "function";
		public static final String ENUMERATION = "enumeration";
		public static final String BRL = "brl";
	}



	public class ModelType {
		public static final String INPUT = "input";
		public static final String OUTPUT = "output";
		public static final String VARIABLE = "Variable";

		public class ENUM {
			public static final String NAME = "name";
			public static final String VALUE = "value";
		}
	}
	
	public class STATE {
		public static final String NORMAL = "00";
		public static final String GET_LIST = "01";
		public static final String GET_LG_EXPIRE = "lgExpire";
	}


}
