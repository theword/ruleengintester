package com.util.rule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.drools.definition.rule.Rule;
import org.drools.event.rule.ActivationCreatedEvent;
import org.drools.event.rule.AfterActivationFiredEvent;
import org.drools.event.rule.AgendaGroupPoppedEvent;
import org.drools.event.rule.AgendaGroupPushedEvent;
import org.drools.event.rule.BeforeActivationFiredEvent;
import org.drools.event.rule.DefaultAgendaEventListener;
import org.drools.runtime.rule.Activation;
import org.drools.runtime.rule.AgendaGroup;

import com.util.rule.constants.RuleConstants;
import com.util.rule.model.RuleControlBean;


@SuppressWarnings("unused")
public class DebugRuleAgendaEventListener extends DefaultAgendaEventListener {
	protected Logger log = Logger.getLogger( DebugRuleAgendaEventListener.class);

	private String id;
	private Map<String,String> dtablePattern = new HashMap<String, String>();
	private List<String> ruleFiredList  =new ArrayList<String>();
	private Boolean IS_FORMAT = Boolean.TRUE;

	public DebugRuleAgendaEventListener(RuleControlBean ruleControlBean){
		this.dtablePattern = ruleControlBean.getDtablePattern();
		this.id = ruleControlBean.getSessionId();
		ruleFiredList.add("["+ruleControlBean.getSessionId()+"]:");
	}
	
	@Override
	public void activationCreated(final ActivationCreatedEvent event) {
		Activation a = event.getActivation();
		Rule rule = a.getRule();		
		String data = "["+id+"]Create ... " + rule.getName();
		ruleFiredList.add("["+rule.getName()+"]-->");
		log.debug(data);
		super.activationCreated(event);
	}
	
	@Override
	public void beforeActivationFired(final BeforeActivationFiredEvent event) {
		Activation a = event.getActivation();
		Rule rule = a.getRule();
		String data = "["+id+"]Before Activation Fired ... " + rule.getName()+"|"+toModelString(a.getObjects());
		log.debug(data);
		log.debug("a.getObjects()  " + a.getObjects());
		super.beforeActivationFired(event);
	}

	@Override
	 public void afterActivationFired(AfterActivationFiredEvent event) {
		Activation a = event.getActivation();
		Rule rule = a.getRule();
		String data = "["+id+"]After Activation Fired ... " + rule.getName()+"|"+toModelString(a.getObjects());
		log.debug(data);
	 }
	
	@Override
	 public void agendaGroupPopped(AgendaGroupPoppedEvent event) {
		AgendaGroup a = event.getAgendaGroup();
		log.debug("["+id+"]Pop Agenda ... " + a.getName());
		ruleFiredList.add("[Pop Agenda:" + a.getName()+"]-->");
		super.agendaGroupPopped(event);
	}
	
	@Override
	 public void agendaGroupPushed(AgendaGroupPushedEvent event)  {
		AgendaGroup a = event.getAgendaGroup();
		log.debug("["+id+"]Push Agenda ... " + a.getName());
		ruleFiredList.add("[Push Agenda:" + a.getName()+"]-->");
		super.agendaGroupPushed(event);
	}
	
	private String toModelString(List<Object> list){
		StringBuffer sb = new StringBuffer();			
		for(Object o:list){
			if(IS_FORMAT){
				String[] strArr = StringUtils.split(o.toString(), ",");
				String newStr = "";
				for(int i=0;i<strArr.length;i++){
					if(i%5==0){
						newStr+=strArr[i]+RuleConstants.LINE_SEPARATOR;
					}else{
						newStr+=strArr[i]+",";
					}
				}			
				sb.append(newStr);
				sb.append(RuleConstants.LINE_SEPARATOR);
			}else{
				sb.append(o.toString());
				sb.append("|");
			}			
		}
		return sb.toString();
	}		

	public List<String> getRuleFiredList() {
		return ruleFiredList;
	}	
	
}
