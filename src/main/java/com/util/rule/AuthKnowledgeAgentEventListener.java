package com.util.rule;

import org.drools.ChangeSet;
import org.drools.event.knowledgeagent.BeforeChangeSetAppliedEvent;
import org.drools.event.rule.DefaultKnowledgeAgentEventListener;
import org.drools.io.Resource;
import org.drools.io.impl.UrlResource;

public class AuthKnowledgeAgentEventListener extends DefaultKnowledgeAgentEventListener {

	private final String username;

	private final String password;

	public AuthKnowledgeAgentEventListener(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public void beforeChangeSetApplied(BeforeChangeSetAppliedEvent event) {
		// Obliged to do this to get UrlResources done correctly...
		ChangeSet changeSet = event.getChangeSet();
		for (Resource res : changeSet.getResourcesAdded()) {
			if (res instanceof UrlResource) {
				setupUrlResource((UrlResource) res);
			}
		}
		for (Resource res : changeSet.getResourcesModified()) {
			if (res instanceof UrlResource) {
				setupUrlResource((UrlResource) res);
			}
		}
		// maybe this is needed for deleted resources, i didn't check

	}

	private void setupUrlResource(UrlResource resource) {
		resource.setBasicAuthentication("enabled");
		resource.setUsername(username);
		resource.setPassword(password);
	}
}
